﻿using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

public class StartManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {        
        
    }

    // Update is called once per frame
    void Update()
    {
       
    }
    public void Start1Player()
    {
        PlayClick();
        GameManager.instance.ResetGameVars();
        // Spawn player
        GameObject p1 = GameObject.FindWithTag("Player");
        p1.SetActive(true);
        MeshRenderer render = p1.gameObject.GetComponentInChildren<MeshRenderer>();
        render.enabled = true;
        GameManager.instance.RespawnPlayer(p1);
        // Set UI objects active/inactive
        GameManager.instance.start.SetActive(false);
        GameManager.instance.game.SetActive(true);
        GameManager.instance.gameOver.SetActive(false);
        GameManager.instance.multiplayer = false;
        GameManager.instance.SetPlayerCameras();
        // Send player 2 to a land far, far away... 
        GameObject p2 = GameObject.FindWithTag("Player2");
        p2.transform.position = new Vector3(9999.0f, 9999.0f, 9999.0f);
    }
    public void Start2Player()
    {
        PlayClick();
        GameManager.instance.ResetGameVars();
        // Spawn players
        GameObject p1 = GameObject.FindWithTag("Player");
        p1.SetActive(true);
        MeshRenderer render = p1.gameObject.GetComponentInChildren<MeshRenderer>();
        render.enabled = true;
        GameManager.instance.RespawnPlayer(p1);        
        GameObject p2 = GameObject.FindWithTag("Player2");
        p2.SetActive(true);
        MeshRenderer render2 = p2.gameObject.GetComponentInChildren<MeshRenderer>();
        render2.enabled = true;
        GameManager.instance.RespawnPlayer(p2);
        // Set UI objects active/inactive
        GameManager.instance.start.SetActive(false);
        GameManager.instance.game.SetActive(true);
        GameManager.instance.gameOver.SetActive(false);
        GameManager.instance.multiplayer = true;
        // Set up player cameras
        GameManager.instance.SetPlayerCameras();
    }
    public void OpenOptions()
    {
        PlayClick();
        Debug.Log("Open Options");
        // Open Options    
        GameManager.instance.options.SetActive(true);
        GameManager.instance.start.SetActive(false);
    }
    public void QuitGame()
    {
        PlayClick();
        Debug.Log("QUIT");
        EditorApplication.isPlaying = false;
        Application.Quit();
    }
    public void PlayClick()
    {
        // Play feedback click
        GameManager.instance.gameAudio.PlayOneShot(GameManager.instance.clickSound, GameManager.instance.sfxVolume);
    }
}
