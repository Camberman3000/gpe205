﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupController : MonoBehaviour
{

    public List<PowerUp> powerups;     
    private TankData tankData;    

    // Start is called before the first frame update
    void Start()
    {
        powerups = new List<PowerUp>();
        tankData = GetComponentInParent<TankData>();
    }

    // Update is called once per frame
    void Update()
    {
        // Expired powerups
        List<PowerUp> expiredPowerups = new List<PowerUp>();

        // Loop through list
        foreach (PowerUp power in powerups)
        {
            // Subtract duration
            power.duration -= Time.deltaTime;

            // If a powerup has expired, add to list
            if (power.duration <= 0)
            {
                expiredPowerups.Add(power);
            }
        }

        // Remove expired powerup(s) from MAIN powerup list and deactivate it
        foreach (PowerUp power in expiredPowerups)
        {
            power.OnDeactivate(tankData);
            powerups.Remove(power);
        }        
        //  Clear TEMP expired powerups list
        expiredPowerups.Clear();
    }

    // Add a powerup to the list and activate it
    public void Add(PowerUp powerup)
    {
        // Activate the powerup
        powerup.OnActivate(tankData);

        // If powerup isn't permanent, add to MAIN powerup list
        if (!powerup.isPermanent)
        {
            powerups.Add(powerup);
        }
    }
}
