﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/************ ENEMY AI CONTROLLER ***********/

public class AIController : MonoBehaviour
{
    public enum AI_Personality
    {
        Chicken,        // Runs from player
        Patrol,         // Uh, normal
        FastAttack,     // Super fast tank
        QueensGuard     // Won't react unless you get very close    
    }
    public enum AI_State
    {
        None,
        Chase,
        ChaseAndFire,       
        Flee,
        CheckForFlee,
        Rest
    };
    public enum LoopType
    {
        Stop,
        Loop,
        PingPong
    };
     
    [Header("AI SENSES")]    
    public AI_Personality personality = AI_Personality.Patrol;
    public AI_State aiState = AI_State.None;
    public LoopType loopType = LoopType.Loop;
    [Space]     

    [Header("AI SENSE VALS")]
    public float fleeDistance = 10.0f;
    public float avoidanceTime = 4.0f;
    public float seeDistance = 100;
    public float hearDistance = 50;
    public float FOV = 45;
     
    private int avoidanceStage = 0;
    private float exitTime;
    public float stateEnterTime;
    //public float aiSenseRadius;

    

    [Header("ATTACK MODS")]
    [Tooltip("Larger number = faster chase speed")]
    public float chaseSpeedModifier = 1;
    [Tooltip("Smaller number = faster firing")]
    public float shootDelayModifier; 
    [Space]
    [Space]

    // Game object refs
    public GameObject target;
    public GameObject target2;
    public Transform targetTF;
    public Transform targetTF2;
    public Transform self;    

    // Component refs
    public Shooter shooter;
    public TankMotor tankMotor;
    public TankData tankData;

    // Tank transform
    private Transform tf;
    // Delay between shots
    public float shootDelay = 2.0f;
    // Travel waypoints
    public Transform[] waypoints = null;
    private int currentWaypoint = 1;
    private bool isPatrolForward = true;
    // acceptable distance from waypoint to allow completion of travel
    public float closeEnough = 1.0f;
    // Flee values
    private float fleeCounter = 0;
    private float fleeLength = 3;
    private Vector3 vectorToTarget;

    public void Awake()
    {
        //Get the transform of the tank
        tf = gameObject.GetComponent<Transform>();       
    }

    // Start is called before the first frame update
    void Start()
    {
        tf = gameObject.GetComponent<Transform>();
    }
        
    void Update()
    {
        // assign target tf
        target = GameObject.FindWithTag("Player");
        target2 = GameObject.FindWithTag("Player2");
        if (GameObject.FindWithTag("Player"))
        {
            targetTF = target.transform;            
        }
        
        if (GameObject.FindWithTag("Player2"))
        {
            targetTF2 = target2.transform;            
        }             

        // Check if in avoidance mode
        if (avoidanceStage != 0)
        {
            // Avoid object
            DoAvoidance();
        }
        else
        {
            if (aiState == AI_State.None)
            {
                // Patrol
            }
            else if (aiState == AI_State.ChaseAndFire)
            {
                DoChase();
            }           
        }
         
        // Rotates tank
        if (tankMotor.RotateTowards(waypoints[currentWaypoint].position, tankData.turnSpeed))
        {
            // Do nothing while tank is rotating             
        }
        else
        {
            //Check for obstacles
            if (CanMove(tankData.moveForwardSpeed))
            {
                // Move forward
                tankMotor.Move(tankData.moveForwardSpeed);
            }
            else
            {
                //Debug.Log("AVOID");
                avoidanceStage = 1;
            }
        }

        //Close to the waypoint?
        if (Vector3.SqrMagnitude(waypoints[currentWaypoint].position - tf.position) < (closeEnough * closeEnough))
        {
            if (loopType == LoopType.Stop)
            {
                // Move to the next waypoint
                if (currentWaypoint < waypoints.Length - 1)
                {
                    currentWaypoint++;
                }
                else
                {
                    //transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y, 0);
                }
            }
            else if (loopType == LoopType.Loop)
            {
                // Move to the next waypoint
                if (currentWaypoint < waypoints.Length - 1)
                {
                    currentWaypoint++;
                }
                else
                {
                    currentWaypoint = 0;
                }
            }
            else if (loopType == LoopType.PingPong)
            {
                if (isPatrolForward)
                {
                    // Move to the next waypoint
                    if (currentWaypoint < waypoints.Length - 1)
                    {
                        currentWaypoint++;
                    }
                    else
                    {
                        // Previous waypoint
                        isPatrolForward = false;
                        currentWaypoint--;
                    }
                }
                else
                {
                    // Move to the previous waypoint
                    if (currentWaypoint < waypoints.Length - 1)
                    {
                        currentWaypoint--;
                    }
                    else
                    {
                        // Next waypoint
                        isPatrolForward = true;
                        currentWaypoint++;
                    }
                }
            }
        }

        switch (personality)
        {
            case AI_Personality.Chicken:
                Chicken_AI();
                break;
            case AI_Personality.Patrol:
                Patrol_AI();
                break;
            case AI_Personality.FastAttack:
                FastAttack_AI();
                break;
            case AI_Personality.QueensGuard:
                QueensGuard_AI();
                break;
        }
    }
    private void Chicken_AI()
    {
        if (CanHear(target) || CanSee(target))
        {
            aiState = AI_State.Flee;
            DoFlee("Target1");
        }

        if (CanHear(target2) || CanSee(target2))
        {
            aiState = AI_State.Flee;
            DoFlee("Target2");
        }
    }
    // Patrols waypoints until player is seen or heard, then attacks. If enemy tank health less than half, it flees
    private void Patrol_AI()
    {
        // Tank health < 50%?
        if (tankData.currentTankHealth <= 50)
        {
            if (CanHear(target) || CanSee(target))
            {
                aiState = AI_State.Flee;
                DoFlee("Target1");
            }

            if (CanHear(target2) || CanSee(target2))
            {
                aiState = AI_State.Flee;
                DoFlee("Target2");
            }
        }
        else
        {
            //Debug.Log("PATROLLING");
            //Debug.Log("TARGET TAG: " + target.tag);
            if (CanHear(target) || CanSee(target))
            {
                //Debug.Log("CAN SEE OR HEAR PLAYER - CHASING");
                aiState = AI_State.ChaseAndFire;
                //Debug.Log("AI STATE: " + aiState);
                // Rotates tank
                if (tankMotor.RotateTowards(targetTF.position, tankData.turnSpeed))
                {
                    // Do nothing while tank is rotating             
                }
                else
                {
                    // Move forward
                    tankMotor.Move(tankData.moveForwardSpeed);
                    // Shoot at player
                    if (shooter.shootEnabled == true)
                    {
                        //Debug.Log("FIRING...");
                        shooter.Shoot();
                    }
                }
            }
            else if (CanHear(target2) || CanSee(target2))
            {
                //Debug.Log("CAN SEE OR HEAR PLAYER 2 - CHASING TARGET...: " + targetTF2.tag);
                aiState = AI_State.ChaseAndFire;
                // Rotates tank
                if (targetTF2)
                {
                    //Debug.Log("ENEMY TANK MOTOR: " + tankMotor);
                    if (tankMotor.RotateTowards(targetTF2.position, tankData.turnSpeed))                    
                    {                        
                        // Do nothing while tank is rotating  
                        //Debug.Log("ROTATING TOWARDS PLAYER 2");
                    }
                    else
                    {
                        // Move forward
                        tankMotor.Move(tankData.moveForwardSpeed);
                        // Shoot at player
                        if (shooter.shootEnabled == true)
                        {
                            //Debug.Log("FIRING...");
                            shooter.Shoot();
                        }
                    }
                }                
            }
            else
            {
                //Debug.Log("CAN'T SEE PLAYER");
                aiState = AI_State.None;
            }
        }
    }

    private void FastAttack_AI()
    {
        // Tank health < 50%?
        if (tankData.currentTankHealth <= 50)
        {
            if (CanHear(target) || CanSee(target))
            {
                aiState = AI_State.Flee;
                DoFlee("Target1");
            }

            if (CanHear(target2) || CanSee(target2))
            {
                aiState = AI_State.Flee;
                DoFlee("Target2");
            }
        }
        else
        {
            //Debug.Log("PATROLLING");
            if (CanHear(target) || CanSee(target))
            {
                //Debug.Log("CAN SEE OR HEAR PLAYER - CHASING");
                aiState = AI_State.ChaseAndFire;
                // Rotates tank
                if (tankMotor.RotateTowards(targetTF.position, tankData.turnSpeed))
                {
                    // Do nothing while tank is rotating             
                }
                else
                {
                    // Move forward at chase speed
                    tankMotor.Move(tankData.moveForwardSpeed * chaseSpeedModifier);

                    // Shoot at player
                    if (shooter.shootEnabled == true)
                    {
                        //Debug.Log("FIRING...");
                        shooter.Shoot();
                    }
                }
            }
            else if (CanHear(target2) || CanSee(target2))
            {
                //Debug.Log("CAN SEE OR HEAR PLAYER - CHASING");
                aiState = AI_State.ChaseAndFire;
                // Rotates tank
                if (targetTF2)
                {
                    if (tankMotor.RotateTowards(targetTF2.position, tankData.turnSpeed))
                    {
                        // Do nothing while tank is rotating             
                    }
                    else
                    {
                        // Move forward at chase speed
                        tankMotor.Move(tankData.moveForwardSpeed * chaseSpeedModifier);

                        // Shoot at player
                        if (shooter.shootEnabled == true)
                        {
                            //Debug.Log("FIRING...");
                            shooter.Shoot();
                        }
                    }
                }               
            }
            else
            {
                //Debug.Log("CAN'T SEE PLAYER");
                aiState = AI_State.None;
            }
        }
    }

    private void QueensGuard_AI() // Doesn't attack unless you are very close, then fires rapidly and will not flee
    {
        if (CanHear(target) || CanSee(target))
        {
            //Debug.Log("CAN SEE OR HEAR PLAYER - CHASING");
            aiState = AI_State.ChaseAndFire;
            // Rotates tank
            if (tankMotor.RotateTowards(targetTF.position, tankData.turnSpeed))
            {
                // Do nothing while tank is rotating             
            }
            else
            {
                // Move forward at chase speed
                tankMotor.Move(tankData.moveForwardSpeed * chaseSpeedModifier);
                // Shoot at player
                if (shooter.shootEnabled == true)
                {
                    //Debug.Log("FIRING...");
                    shooter.fireDelay = (shooter.fireDelay * shootDelayModifier);
                    shooter.Shoot();
                }
            }
        }
        else if (CanHear(target2) || CanSee(target2))
        {
            //Debug.Log("CAN SEE OR HEAR PLAYER - CHASING");
            aiState = AI_State.ChaseAndFire;
            // Rotates tank
            if (targetTF2)
            {
                if (tankMotor.RotateTowards(targetTF2.position, tankData.turnSpeed))
                {
                    // Do nothing while tank is rotating             
                }
                else
                {
                    // Move forward at chase speed
                    tankMotor.Move(tankData.moveForwardSpeed * chaseSpeedModifier);
                    // Shoot at player
                    if (shooter.shootEnabled == true)
                    {
                        //Debug.Log("FIRING...");
                        shooter.fireDelay = (shooter.fireDelay * shootDelayModifier);
                        shooter.Shoot();
                    }
                }
            }           
        }
        else
        {
            //Debug.Log("CAN'T SEE PLAYER");
            aiState = AI_State.None;
        }
    }

    void DoChase()
    {
        if (CanHear(target) || CanSee(target))
        {
            if (targetTF != null)
            {
                tankMotor.RotateTowards(targetTF.position, tankData.turnSpeed);
                // Check if we can move "data.moveSpeed" units away.
                //    We chose this distance, because that is how far we move in 1 second,
                //    This means, we are looking for collisions "one second in the future."
                if (CanMove(tankData.moveForwardSpeed))
                {
                    tankMotor.Move(tankData.moveForwardSpeed);
                }
                else
                {
                    // Enter obstacle avoidance stage 1
                    avoidanceStage = 1;
                }
            }
        }
        else if (CanHear(target2) || CanSee(target2))
        {
            if (targetTF2 != null)
            {
                tankMotor.RotateTowards(targetTF2.position, tankData.turnSpeed);
                // Check if we can move "data.moveSpeed" units away.
                //    We chose this distance, because that is how far we move in 1 second,
                //    This means, we are looking for collisions "one second in the future."
                if (CanMove(tankData.moveForwardSpeed))
                {
                    tankMotor.Move(tankData.moveForwardSpeed);
                }
                else
                {
                    // Enter obstacle avoidance stage 1
                    avoidanceStage = 1;
                }
            }
        }        
    }
    private void DoFlee(string v)
    {
        if (v == "Player1")
        {
            // Vector from self to target
              vectorToTarget = targetTF.position - self.position;
        }
        else if (v == "Player2")
        {
            // Vector from self to target
             vectorToTarget = targetTF2.position - self.position;
        }       

        // Vector away from target
        Vector3 vectorAwayFromTarget = -1 * vectorToTarget;

        // Normalize vector to give it a magnitude of 1
        vectorAwayFromTarget.Normalize();

        // Flee length
        vectorAwayFromTarget *= fleeDistance;

        // Flee distance from our current position
        Vector3 fleePosition = vectorAwayFromTarget + self.position;
        tankMotor.RotateTowards(fleePosition, tankData.turnSpeed);
        tankMotor.Move(tankData.moveForwardSpeed);

        // Allow tank to flee if fleeCounter >= fleeDelay time. 
        fleeCounter += Time.deltaTime;
        if (fleeCounter >= fleeLength)
        {
            // Reset shoot enabled so player can fire again.
            aiState = AI_State.None;
            // Reset counter after fleeDelay time has elapsed
            fleeCounter = 0.0f;
        }        
    }
       
    // DoAvoidance - handles obstacle avoidance
    void DoAvoidance()
    {
        if (avoidanceStage == 1)
        {
            // Rotate left
            tankMotor.Rotate(-1 * tankData.turnSpeed / 30);            
            // If I can now move forward, move to stage 2!
            if (CanMove(tankData.moveForwardSpeed))
            {
                avoidanceStage = 2;
                // Set the number of seconds we will stay in Stage2
                exitTime = avoidanceTime;
            }
            // Otherwise, we'll do this again next turn!
        }
        else if (avoidanceStage == 2)
        {
            // if we can move forward, do so
            if (CanMove(tankData.moveForwardSpeed))
            {
                // Subtract from our timer and move
                exitTime -= Time.deltaTime;
                tankMotor.Move(tankData.moveForwardSpeed);
                // If we have moved long enough, return to chase mode
                if (exitTime <= 0)
                {
                    avoidanceStage = 0;
                }
            }
            else
            {
                // Otherwise, we can't move forward, so back to stage 1
                avoidanceStage = 1;
            }
        }
    }
      
    public bool CanMove(float speed)
    {
        // Cast a ray forward in the distance that we sent in
        RaycastHit hit;

        // If our raycast hit something...
        if (Physics.Raycast(tf.position, tf.forward, out hit, speed))
        {
            // ... and if what we hit is not the player...
            if (!hit.collider.CompareTag("Player"))
            {
                // ... then we can't move
                return false;
            }
        }
        // otherwise, we can move, so return true
        return true;
    }

    public bool CanSee(GameObject target)
    {        
        if (target)
        {             
            //Find the angle to target
            Vector3 vectorToTarget = target.transform.position - tf.position;
            float angleToTarget = Vector3.Angle(tf.forward, vectorToTarget);

            //If < FOV, then check see
            if (angleToTarget < FOV)
            {                
                //Check for LOS
                Ray myRay = new Ray();
                RaycastHit hitInfo = new RaycastHit();
                myRay.origin = tf.position;
                myRay.direction = vectorToTarget;
                if (Physics.Raycast(myRay, out hitInfo, seeDistance))
                {
                    if (hitInfo.collider.gameObject == target)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    // Can't see
                    return false;
                }
            }
            else
            {
                //  Not in FOV
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public bool CanHear(GameObject target)
    {
        if (target)
        {
            if (Vector3.Distance(target.transform.position, tf.position) < hearDistance)
            {               
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            // Can't hear
            return false;
        }
    }
}
