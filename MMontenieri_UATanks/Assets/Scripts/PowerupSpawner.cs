﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupSpawner : MonoBehaviour
{
    public List<GameObject> powerups;   
    public float spawnDelay;
    private GameObject spawnedPickup;
    private float nextSpawnTime;
    private Transform tf;

    // Start is called before the first frame update
    void Start()
    {
        tf = gameObject.GetComponent<Transform>();
        nextSpawnTime = Time.time + spawnDelay;
    }

    // Update is called once per frame
    void Update()
    {
        // If null then spawn
        if (spawnedPickup == null)
        {
            // Enough time elapsed to spawn?
            if (Time.time > nextSpawnTime)
            {
                // Spawn & set next spawn time
                GameObject powerup = powerups[UnityEngine.Random.Range(0, powerups.Count)];
                spawnedPickup = Instantiate(powerup, tf.position, Quaternion.identity) as GameObject;                 
                nextSpawnTime = Time.time + spawnDelay;
            }
        }
        else
        {
            // Object exists, so postpone the spawn
            nextSpawnTime = Time.time + spawnDelay;
        }
    }
}
