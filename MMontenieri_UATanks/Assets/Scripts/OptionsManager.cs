﻿using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionsManager : MonoBehaviour
{
    public Text musicVolNumText;
    public Text sfxVolNumText;
    private float musicVolNum;
    private float sfxVolNum;    
    private string musicVolEntry = "Music_Vol";
    private string sfxVolEntry = "SFX_Vol";
    private string modEntry = "Map_Of_The_Day";
    private int modYes = 0;

    private void Awake()
    {
        /* Note: Moved this to Awake from Start since we want this to run
           even if the player doesn't open Options - (Start only runs if you 
            open Options) */
        // See if playerprefs has an entry for music volume        
        if (PlayerPrefs.HasKey(musicVolEntry))
        {
            musicVolNum = PlayerPrefs.GetFloat(musicVolEntry);
            GameManager.instance.musicVolume = musicVolNum;
        }
        else
        {
            // Entry not found. Use default
            musicVolNum = GameManager.instance.musicVolume;
        }
        // Update UI
        musicVolNumText.text = musicVolNum.ToString();

        // See if playerprefs has an entry for sfx volume        
        if (PlayerPrefs.HasKey(sfxVolEntry))
        {
            sfxVolNum = PlayerPrefs.GetFloat(sfxVolEntry);
            GameManager.instance.sfxVolume = sfxVolNum;
        }
        else
        {
            // Entry not found. Use default
            sfxVolNum = GameManager.instance.sfxVolume;
        }
        // Update UI
        sfxVolNumText.text = sfxVolNum.ToString();

        // See if playerprefs has an entry for map of the day        
        if (PlayerPrefs.HasKey(modEntry))
        {
            modYes = PlayerPrefs.GetInt(modEntry);
            if (modYes == 1)
            {
                // Toggle map of day on
                //Debug.Log("TOGGLE MAP OF THE DAY");
                GameManager.instance.mapOfDayToggle.isOn = true;
            }
            else
            {
                // Leave unchecked
            }
        }
        else
        {
            // Entry not found. Map of day remains unchecked            
        }
    }

    // Start is called before the first frame update
    void Start()
    {       
       
    }

    // Update is called once per frame
    void Update()
    {
        // game music volume update         
        GameManager.instance.gameAudio.volume = (GameManager.instance.musicVolume * 0.1f);
    }

    public void TurnMusicVolumeUp()
    {
        //Debug.Log("Music up");
        musicVolNum = int.Parse(musicVolNumText.text);
        musicVolNum++;
       
        if (musicVolNum > 10)
        {
            musicVolNum = 10;
            musicVolNumText.text = "10";
        }
        musicVolNumText.text = musicVolNum.ToString();
        GameManager.instance.musicVolume = musicVolNum;
        PlayClick();
        // Save volume
        PlayerPrefs.SetFloat(musicVolEntry, musicVolNum);
        PlayerPrefs.Save();
    }

    public void TurnMusicVolumeDown()
    {
        //Debug.Log("Music down");
        musicVolNum = int.Parse(musicVolNumText.text);
        musicVolNum--;        
        
        if (musicVolNum < 1)
        {
            musicVolNum = 0;
            musicVolNumText.text = "0";
        }
        musicVolNumText.text = musicVolNum.ToString();
        GameManager.instance.musicVolume = musicVolNum;
        PlayClick();
        // Save volume
        PlayerPrefs.SetFloat(musicVolEntry, musicVolNum);
        PlayerPrefs.Save();
    }

    public void TurnSFXVolumeUp()
    {
        //Debug.Log("SFX up");
        sfxVolNum = int.Parse(sfxVolNumText.text);
        sfxVolNum++;

        if (sfxVolNum > 10)
        {
            sfxVolNum = 10;
            sfxVolNumText.text = "10";
        }
        sfxVolNumText.text = sfxVolNum.ToString();
        GameManager.instance.sfxVolume = sfxVolNum;
        PlayClick();
        // Save volume
        PlayerPrefs.SetFloat(sfxVolEntry, sfxVolNum);
        PlayerPrefs.Save();
    }

    public void TurnSFXVolumeDown()
    {
        //Debug.Log("SFX down");
        sfxVolNum = int.Parse(sfxVolNumText.text);
        sfxVolNum--;

        if (sfxVolNum < 1)
        {
            sfxVolNum = 0;
            sfxVolNumText.text = "0";
        }
        sfxVolNumText.text = sfxVolNum.ToString();
        GameManager.instance.sfxVolume = sfxVolNum;
        PlayClick();
        // Save volume
        PlayerPrefs.SetFloat(sfxVolEntry, sfxVolNum);
        PlayerPrefs.Save();
    }

    public void ReturnToStartMenu()
    {
        GameManager.instance.options.SetActive(false);
        GameManager.instance.start.SetActive(true);
        PlayClick();
    }
    public void MapOfDayToggle()
    {
        if (GameManager.instance.mapOfDayToggle.isOn)
        {
            //Debug.Log("MAP OF DAY CHECKED");
            GameManager.instance.mapOfTheDay = true;            
            PlayerPrefs.SetInt(modEntry, 1);
            PlayerPrefs.Save();
        }
        else
        {
            //Debug.Log("MAP OF DAY UN-CHECKED");
            GameManager.instance.mapOfTheDay = false;
            PlayerPrefs.SetInt(modEntry, 0);
            PlayerPrefs.Save();
        }
        PlayClick();
    }
    public void PlayClick()
    {
        // Play feedback click
        GameManager.instance.gameAudio.PlayOneShot(GameManager.instance.clickSound, GameManager.instance.sfxVolume);
    }
}
