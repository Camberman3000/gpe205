﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/************ PLAYER INPUT CONTROLLER ***********/

public class InputController : MonoBehaviour
{
    // Shooter ref
    public Shooter shooter;
    // Tank motor ref
    public TankMotor motor;
    // Tank data ref
    public TankData tankData;
    // Input key for firing the tank
    public KeyCode shootKey = KeyCode.Space;
    // Input key for rotating the turret (only goes left, this was just proof of concept)
    public KeyCode turretKey = KeyCode.T;
    // Tank cannon transform
    public Transform tankCannon;
    // Score
    private ScoreData m_score;

    // Input options: Can add controller/gamepad if desired
    public enum InputScheme 
    { WSAD, 
      arrowKeys
    };
    public InputScheme input = InputScheme.WSAD;

    // Start is called before the first frame update
    public void Start()
    {
        if (GameManager.instance)
        {
            GameManager.instance.playerControllers.Add(this);
        }
        m_score = new ScoreData();
    }

    public void OnDestroy()
    {
        if (GameManager.instance)
        {
            GameManager.instance.playerControllers.Remove(this);
        }
    }

    // Update is called once per frame
    void Update()
    {
        /************ DEBUG KEYS ***************/
        if (Input.GetKey(KeyCode.O))
        {
            // Open Options    
            GameManager.instance.options.SetActive(true);
            GameManager.instance.game.SetActive(false);
        }
        if (Input.GetKey(KeyCode.P))
        {
            // Close Options    
            GameManager.instance.options.SetActive(false);
            GameManager.instance.game.SetActive(true);
        }
        if (Input.GetKey(KeyCode.G))
        {
            // Force game over screen    
            GameManager.instance.game.SetActive(false);
            GameManager.instance.gameOver.SetActive(true);
        }
        if (Input.GetKey(KeyCode.Y))
        {
            // Delete playerprefs   
            PlayerPrefs.DeleteAll();
        }
        if (Input.GetKey(KeyCode.F6))
        {            
            m_score.score = PlayerPrefs.GetInt("High_Score_1", 0);
            Debug.Log("Score 1: " + m_score.score);

            m_score.score = PlayerPrefs.GetInt("High_Score_2", 0);
            Debug.Log("Score 2: " + m_score.score);

           m_score.score = PlayerPrefs.GetInt("High_Score_3", 0);
            Debug.Log("Score 3: " + m_score.score);
        }
        
        // Call RotateTurret each time the turretKey is pressed
        if (Input.GetKeyDown(turretKey))
        {
            RotateTurret();
        }
        // Handle user input
        switch (input)
        {
            // Arrow keys
            case InputScheme.arrowKeys:
                if (Input.GetKey(KeyCode.UpArrow))
                {
                    motor.Move(tankData.moveForwardSpeed);
                }
                if (Input.GetKey(KeyCode.DownArrow))
                {
                    motor.Move(-tankData.moveReverseSpeed);
                }
                if (Input.GetKey(KeyCode.RightArrow))
                {
                    motor.Rotate(tankData.turnSpeed * Time.deltaTime);
                }
                if (Input.GetKey(KeyCode.LeftArrow))
                {
                    motor.Rotate(-tankData.turnSpeed * Time.deltaTime);
                }
                if (Input.GetKeyDown(KeyCode.KeypadPlus))
                {
                    GameManager.instance.whoShot = "Player2";
                    shooter.Shoot();
                }
                break;

                // WSAD keys
            case InputScheme.WSAD:
                if (Input.GetKey(KeyCode.W))
                {
                    motor.Move(tankData.moveForwardSpeed);
                }
                if (Input.GetKey(KeyCode.S))
                {
                    motor.Move(-tankData.moveReverseSpeed);
                }
                if (Input.GetKey(KeyCode.A))
                {
                    motor.Rotate(-tankData.turnSpeed * Time.deltaTime);
                }
                if (Input.GetKey(KeyCode.D))
                {                     
                    motor.Rotate(tankData.turnSpeed * Time.deltaTime);
                }
                if (Input.GetKeyDown(shootKey))
                {
                    GameManager.instance.whoShot = "Player";
                    shooter.Shoot();
                }
                break; 
        } 
    }

    // Rotate the turret
    public void RotateTurret()
    {
        // Declare vector and assign to Vector3.up - this will rotate the tank cannon left or right only
        Vector3 rotateVector = Vector3.up;
        // Determine rotation amt. Note: Replace with var to enable changes in inspector
        rotateVector *= -10;
        // Rotate the cannon at the desired speed and in the desired direction
        tankCannon.Rotate(rotateVector, Space.Self);
    }
}
