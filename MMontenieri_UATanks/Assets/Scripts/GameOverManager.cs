﻿using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverManager : MonoBehaviour
{
    public Text highScore1Text;
    public Text highScore2Text;
    public Text highScore3Text;
    private int highScore1Num;
    private int highScore2Num;
    private int highScore3Num;   

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.instance.game.activeSelf)
        {
            highScore1Text.text = GameManager.instance.highScore1.ToString();
            highScore2Text.text = GameManager.instance.highScore2.ToString();
            highScore3Text.text = GameManager.instance.highScore3.ToString();
        }             
    }

    public void RestartGame()
    {
        GameManager.instance.ResetGameVars();
        GameManager.instance.gameOver.SetActive(false);
        GameManager.instance.start.SetActive(true);
        GameManager.instance.game.SetActive(true);
        PlayClick();
    }
    public void PlayClick()
    {
        // Play feedback click
        GameManager.instance.gameAudio.PlayOneShot(GameManager.instance.clickSound, GameManager.instance.sfxVolume);
    }
}
