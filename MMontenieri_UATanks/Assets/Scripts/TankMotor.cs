﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/************ TANK MOTOR ***********/

public class TankMotor : MonoBehaviour
{
    // Character Controller
    private CharacterController characterController;
    // Tank transform 
    private Transform tf;

    public void Awake()
    {
        //Get the transform of the tank
        tf = gameObject.GetComponent<Transform>();
    }
    // Start is called before the first frame update
    void Start()
    {
        // Assign CharacterController component to characterController
        characterController = gameObject.GetComponent<CharacterController>();        
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    // Tank linear movement
    public void Move(float speed)
    {
        // Declare vector and assign to transform.forward - this locks the movement into a forward or backward motion only
        Vector3 tankSpeedVector = tf.forward;                 
        // Determine movement speed according to passed in value
        tankSpeedVector *= speed;
        // Move the tank at the desired speed and in the desired direction
        characterController.SimpleMove(tankSpeedVector);

         
    }

    // Tank rotation
    public void Rotate(float speed)
    {
        // Declare vector and assign to Vector3.up - this will rotate the tank left or right only
        Vector3 rotateVector = Vector3.up;
        // Determine rotation speed according to passed in value
        rotateVector *= speed;
        // Rotate the tank at the desired speed and in the desired direction
        tf.Rotate(rotateVector, Space.Self);
    }

    // Rotate towards the target
    // If can rotate, return true. If can't rotate, return false.
    public bool RotateTowards(Vector3 target, float speed)
    {
        Vector3 vectorToTarget;
        //Vector to our target = diff between target position and our position.        
        vectorToTarget = target - tf.position;

        if (vectorToTarget != Vector3.zero)
        {
             
            //Find the quaternion that looks down the vector
            Quaternion targetRotation = Quaternion.LookRotation(vectorToTarget);

            // Rotate complete?
            if (targetRotation == tf.rotation)
            {
                return false;
            }

            //Change our rotation so that we are closer to our target rotation, but never turn faster than our Turn Speed
            //Use Time.deltaTime for "Degrees per Second"          
            tf.rotation = Quaternion.RotateTowards(tf.rotation, targetRotation, speed * Time.deltaTime);          
        }
        // We rotated, so return true
        return true;
    }
}

