﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    public PowerUp powerup;
    public AudioClip feedback;
    public float defaultSoundEffectsVolume = 0.5f;

    private Transform tf;    
    // Start is called before the first frame update
    void Start()
    {
        tf = gameObject.GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter(Collider other)
    {       
        // Tank PowerupController ref 
        PowerupController powCon = other.gameObject.GetComponent<PowerupController>();
        //Debug.Log("Other game object: " + other.gameObject);
        // Does tank (other object) have a PowerupController?
        if (powCon != null)
        {            
            // Add the powerup
            powCon.Add(powerup);
            // Play feedback (if set)
            if (feedback != null)
            {                
                // Play sound at the specified volume              
                AudioSource.PlayClipAtPoint(feedback, tf.position, 1.0f);
            }
            // Destroy the pickup
            Destroy(gameObject);
        }
    }
}
