﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/************ TANK DATA ***********/

public class TankData : MonoBehaviour
{
    
    // Player/Enemy ref - Used to get the damage per hit from the player (as set in inspector)
    public GameObject selfRef;   
    public GameObject enemyRef;
     
    // TankData refs
    private TankData selfTankData;
    private TankData enemyTankData;
    private TankData enemyTankData2;

    // Tank prefab
    public GameObject tankPF;
    // Tank motor ref
    public TankMotor tankMotor;
    // Shooter ref
    public Shooter shooter;
    // Forward speed
    public float moveForwardSpeed = 5;
    // Reverse speed
    public float moveReverseSpeed = 5;
    // Turn Left/Right speed
    public float turnSpeed = 1;
    // Current tank health
    public float currentTankHealth = 100;
    // Max tank health 
    public float maxTankHealth = 100;    
    // how many points the tank gets per kill
    public int pointsPerKill = 0;
    // Sounds
    public AudioClip deathSound;
    public AudioClip projectileImpactSound;

    private ScoreData scoreData;
    private GameObject p1;
    private GameObject p2;
    private TankData p1TankData;
    private TankData p2TankData;
    [Header("REGEN")]
    public float restingHealRate; // in hp/second 
     


    // Start is called before the first frame update
    void Start()
    {
        //GM = GameObject.FindWithTag("GameManager").GetComponent<GameManager>();
        scoreData = new ScoreData();
    }

    // Update is called once per frame
    void Update()
    {
        // Find Players
        p1 = GameObject.FindWithTag("Player");
        p2 = GameObject.FindWithTag("Player2");
         
        // Get TankData
        p1TankData = p1.GetComponent<TankData>();
        p2TankData = p2.GetComponent<TankData>();

        // Update UI
        GameManager.instance.p1HealthText.text = p1TankData.currentTankHealth.ToString("F0"); // Don't show any decimal places ("F0")
        GameManager.instance.p2HealthText.text = p2TankData.currentTankHealth.ToString("F0");        

        // Heal appropriate player
        if (GameObject.FindWithTag("Player"))
        {
            //Debug.Log("HEAL");
            DoHealthRegen(selfTankData);
        }

        if (GameObject.FindWithTag("Player2"))
        {            
            DoHealthRegen(p2TankData);
        }


    }
    public void DoHealthRegen(TankData tankToHeal)  
    {
         
        // Increase our health. Remember that our increase is "per second"!
        tankToHeal.currentTankHealth += restingHealRate * Time.deltaTime;

        // But never go over our max health
        tankToHeal.currentTankHealth = Mathf.Min(tankToHeal.currentTankHealth, tankToHeal.maxTankHealth);
    }

    private void OnTriggerEnter(Collider other)
    {
        // Get ref to players and enemies
        if (GameManager.instance.whoShot == "Player")
        {
            selfRef = GameObject.FindWithTag("Player");
        }
        else if (GameManager.instance.whoShot == "Player2")
        {
            selfRef = GameObject.FindWithTag("Player2");
        }
        enemyRef = GameObject.FindWithTag("Enemy");

        //Get TankData ref        
        selfTankData = selfRef.GetComponent<TankData>();
        enemyTankData = enemyRef.GetComponent<TankData>();

        // Is projectile 
        if (other.gameObject.tag == "Projectile")
        {
            // HIT THE ENEMY
            if (this.gameObject.tag == "Enemy")
            {
                // Play impact sound
                GameManager.instance.gameAudio.PlayOneShot(projectileImpactSound, GameManager.instance.sfxVolume);
                //Debug.Log("DMG PER HIT FROM PLAYER REF " + selfRef.GetComponent<Shooter>().damagePerHit);                 
                //Debug.Log("selfRef: " + selfRef);
                // Destroy the projectile
                Destroy(other.gameObject);
                
                // Update enemy health
                enemyRef.GetComponent<TankData>().currentTankHealth -= selfRef.GetComponent<Shooter>().damagePerHit;

                // Is enemy health <= 0 ?
                if (enemyRef.GetComponent<TankData>().currentTankHealth <= 0)
                {
                    // Play death sound
                    GameManager.instance.gameAudio.PlayOneShot(deathSound, GameManager.instance.sfxVolume);
                    enemyRef.GetComponent<TankData>().currentTankHealth = 0;
                    //Debug.Log("He's dead, Jim");
                    // Remove from game manager list
                    //GameManager.instance.enemies.Remove(this.GetComponent<TankData>());
                    // Destroy enemy tank
                    //Destroy(this.gameObject);
                    // Respawn enemy tank
                    GameManager.instance.RespawnPlayer(this.gameObject);
                    // Update Score
                    pointsPerKill = selfRef.GetComponent<TankData>().pointsPerKill;
                    if (GameManager.instance.whoShot == "Player")
                    {
                        GameManager.instance.player1Score += pointsPerKill;
                    }
                    else if (GameManager.instance.whoShot == "Player2")
                    {
                        GameManager.instance.player2Score += pointsPerKill;
                    }

                    
                }
            }
            // HIT PLAYER 1
            if (this.gameObject.tag == "Player")
            {
                 
                //Debug.Log("DMG PER HIT FROM ENEMY REF " + enemyRef.GetComponent<Shooter>().damagePerHit);                 
                //Debug.Log("HIT PLAYER");                 
                // Destroy projectile
                Destroy(other.gameObject);
                // Update player health
                //Debug.Log("PLAYER TANK DATA: " + playerTankData);
                //Debug.Log("ENEMY TANK DATA: " + enemyTankData);
                //Debug.Log("DAMAGE PER HIT: " + enemyTankData.GetComponent<Shooter>().damagePerHit);
                selfTankData.currentTankHealth -= enemyTankData.GetComponent<Shooter>().damagePerHit;
                // Is player health <= 0?
                if (selfTankData.currentTankHealth <= 0)
                {
                    // Play death sound
                    GameManager.instance.gameAudio.PlayOneShot(deathSound, GameManager.instance.sfxVolume);
                    selfTankData.currentTankHealth = 0;
                    // Update Score
                    pointsPerKill = enemyTankData.pointsPerKill;
                    GameManager.instance.enemyScore += pointsPerKill;
                    //Debug.Log("NEW ENEMY SCORE: " + GM.enemyScore);
                    //Debug.Log("Player dead");                    
                    // Update lives remaining
                    GameManager.instance.player1LivesRemaining = (GameManager.instance.player1LivesRemaining - 1);
                    if (GameManager.instance.multiplayer == true)
                    {
                        GameOver2PlayerCheck();
                    }
                    else
                    {
                        GameOverSinglePlayerCheck();                       
                    }                             
                }
            }
            // HIT PLAYER 2
            else if (this.gameObject.tag == "Player2")
            {
                
                //Debug.Log("DMG PER HIT FROM ENEMY REF " + enemyRef.GetComponent<Shooter>().damagePerHit);                 
                //Debug.Log("HIT PLAYER 2");                 
                // Destroy projectile
                Destroy(other.gameObject);
                // Update player health
                //Debug.Log("PLAYER TANK DATA: " + playerTankData);
                //Debug.Log("ENEMY TANK DATA: " + enemyTankData);
                //Debug.Log("DAMAGE PER HIT: " + enemyTankData.GetComponent<Shooter>().damagePerHit);
                selfTankData.currentTankHealth -= enemyTankData.GetComponent<Shooter>().damagePerHit;
                // Is player health <= 0?
                if (selfTankData.currentTankHealth <= 0)
                {
                    // Play death sound
                    GameManager.instance.gameAudio.PlayOneShot(deathSound, GameManager.instance.sfxVolume);
                    selfTankData.currentTankHealth = 0;
                    // Update Score
                    pointsPerKill = enemyTankData.pointsPerKill;
                    GameManager.instance.enemyScore += pointsPerKill;
                    //Debug.Log("NEW ENEMY SCORE: " + GM.enemyScore);
                    Debug.Log("Player 2 dead");
                    // Update lives remaining
                    GameManager.instance.player2LivesRemaining = (GameManager.instance.player2LivesRemaining - 1);
                    if (GameManager.instance.multiplayer == true)
                    {
                        GameOver2PlayerCheck();
                    }
                    else
                    {
                        GameOverSinglePlayerCheck();
                    }
                }
            }
        }       
    }

    private void GameOverSinglePlayerCheck()
    {
        if (GameManager.instance.player1LivesRemaining == 0)
        {
            // Game over
            //Debug.Log("Game over");
            GameObject p1 = GameObject.FindWithTag("Player");             
            p1.transform.position = new Vector3(9999.0f, 9999.0f, 9999.0f);
            MeshRenderer render = p1.gameObject.GetComponentInChildren<MeshRenderer>();
            render.enabled = false;
            GameManager.instance.gameOver.SetActive(true);
            GameManager.instance.game.SetActive(false);

            // Update high scores
           
            // Set scoreData score
            scoreData.score = GameManager.instance.player1Score;
            //Debug.Log("PLAYER 1 SCORE: " + GameManager.instance.player1Score);
            // Pass scoreData to UpdateScores()
            GameManager.instance.UpdateScores(scoreData);
            // Show score on game over screen
            GameManager.instance.p1GameOverScoreText.text = scoreData.score.ToString();
        }
        else
        {
            // Respawn player, they have lives remaining
            GameManager.instance.RespawnPlayer(this.gameObject);
            selfTankData.currentTankHealth = selfTankData.maxTankHealth;
            // Reset player health to max
        }
    }

    private void GameOver2PlayerCheck()
    {
        GameObject p1 = GameObject.FindWithTag("Player");
        GameObject p2 = GameObject.FindWithTag("Player2");

        if (GameManager.instance.player1LivesRemaining == 0 && GameManager.instance.player2LivesRemaining == 0)
        {
            // Game over
            Debug.Log("Game over!!");

            // Teleport and disable render on both players
            p1.transform.position = new Vector3(9999.0f, 9999.0f, 9999.0f);
            MeshRenderer render = p1.gameObject.GetComponentInChildren<MeshRenderer>();
            render.enabled = false;
            p1.SetActive(false);

            p2.transform.position = new Vector3(9999.0f, 9999.0f, 9999.0f);
            MeshRenderer render2 = p2.gameObject.GetComponentInChildren<MeshRenderer>();
            render2.enabled = false;
            p2.SetActive(false);

            GameManager.instance.gameOver.SetActive(true);
            GameManager.instance.game.SetActive(false);
            // Set scoreData score - Only the highest score in a 2-player game will count towards the games high score records
            scoreData.score = Mathf.Max(GameManager.instance.player1Score, GameManager.instance.player2Score);
            // Pass scoreData to UpdateScores()
            GameManager.instance.UpdateScores(scoreData);

            // Show score on game over screen
            GameManager.instance.p1GameOverScoreText.text = GameManager.instance.player1Score.ToString();             
            GameManager.instance.p2GameOverScoreText.text = GameManager.instance.player2Score.ToString();
        }
        else
        {
            //Debug.Log("Game not over!");
            if (this.gameObject.tag == "Player")
            {
                if (GameManager.instance.player1LivesRemaining == 0)
                {
                    // Disable player 1, they have no lives remaining
                    p1.transform.position = new Vector3(9999.0f, 9999.0f, 9999.0f);
                    MeshRenderer render = p1.gameObject.GetComponentInChildren<MeshRenderer>();
                    render.enabled = false;
                    p1.SetActive(false);
                }
                else
                {
                    // Respawn player, they have lives remaining
                    GameManager.instance.RespawnPlayer(p1);
                    // Reset player health to max
                    selfTankData.currentTankHealth = selfTankData.maxTankHealth;
                }
            }

            if (this.gameObject.tag == "Player2")
            {
                if (GameManager.instance.player2LivesRemaining == 0)
                {
                    // Disable player 2, they have no lives remaining                     
                    p2.transform.position = new Vector3(9999.0f, 9999.0f, 9999.0f);
                    MeshRenderer render2 = p2.gameObject.GetComponentInChildren<MeshRenderer>();
                    render2.enabled = false;
                    p2.SetActive(false);
                }
                else
                {
                    // Respawn player, they have lives remaining
                    GameManager.instance.RespawnPlayer(p2);
                    // Reset player health to max
                    selfTankData.currentTankHealth = selfTankData.maxTankHealth;
                }
            }
        }
    } 
}
