﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PowerUp 
{
    // Powerup info
    public float speedModifier;
    public float healthModifier;
    public float maxHealthModifier;
    public float fireRateModifier;
    public float duration;
    public bool isPermanent;
    public AudioClip powerupActivated;
    public AudioClip powerupDeactivated;

    public void OnActivate(TankData target)
    {
        //Debug.Log("HOHOHO");
        // Play powerup sound
        GameManager.instance.gameAudio.PlayOneShot(powerupActivated, GameManager.instance.sfxVolume);
        //Debug.Log("Target: " + target + " Speed modifier: " + speedModifier);
        target.moveForwardSpeed += speedModifier;
        target.currentTankHealth += healthModifier;
        target.maxTankHealth += maxHealthModifier;
        target.shooter.fireDelay += fireRateModifier;
    }

    public void OnDeactivate(TankData target)
    {
        // Play powerup deactivated sound
        GameManager.instance.gameAudio.PlayOneShot(powerupDeactivated, GameManager.instance.sfxVolume);
        target.moveForwardSpeed -= speedModifier;
        target.currentTankHealth -= healthModifier;
        target.maxTankHealth -= maxHealthModifier;
        target.shooter.fireDelay -= fireRateModifier;
    }
}
