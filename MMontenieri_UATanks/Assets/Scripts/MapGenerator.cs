﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MapGenerator : MonoBehaviour
{    
    //public GameObject game;
    public GameObject PowerupSpawnerPrefab;
    public GameObject playerSpawnPoint;
    public GameObject enemySpawnPoint;
    public GameObject waypointSpawnPoint;    
    // Designer can change number of rows/columns
    public int rows;
    public int columns;
    // Designer can set map seed to non-random num
    public int mapSeed;
    // bool
    public bool isRandomMap;
    private float roomWidth = 50.0f;
    private float roomHeight = 50.0f;
    public GameObject[] gridPrefabs;
    public List<GameObject> enemyTanks;
    public GameObject waypoint;
    private Room[,] grid;
    
    private int tankCounter = 0;
    private int playerCounter;
    public GameObject playerTank;
     
    private GameObject player;
     
    private CameraFollowRotate p1camFAR;
    private CameraFollowRotate p2camFAR;

    private GameObject enemy;   
   
    
    private GameObject[] waypoints;
    
    private int roomGenIterations;
    private GameObject waypointSpawner;
    private float xPosition;
    private float zPosition;
    // Gets a 2nd random number seed for enemy spawn point generation
    private int espCounter = 0;
    private int randWPLoc;
    private int randWPLoc2;
    private GameObject[] enemies;

    // Start is called before the first frame update
    void Start()
    {
        if (GameManager.instance.mapOfTheDay == true)
        {            
            mapSeed = DateToInt(DateTime.Now.Date); // New seed each day
        }
        else if (isRandomMap)
        {
            mapSeed = DateToInt(DateTime.Now);
        }
        else if (mapSeed == 0)
        {
            mapSeed = DateToInt(DateTime.Now);
        }
        else
        {
            // Use seed input by the designer
        }
        roomGenIterations = 0;
        espCounter = 0;
        GenerateGrid();       
    }

    // Update is called once per frame
    void Update()
    {
         
    }
    public void GenerateGrid()
    {
        // Set map seed
        UnityEngine.Random.InitState(mapSeed);

        // Clear the grid - "which column" is our X, "which row" is our Y
        grid = new Room[columns, rows];

        // For each grid row...
        for (int row = 0; row < rows; row++)
        {
            // For each column in that row
            for (int column = 0; column < columns; column++)
            {
                // Track number of rooms generated
                roomGenIterations++;

                // Figure out location
                 xPosition = roomWidth * column;
                 zPosition = roomHeight * row;
                Vector3 newPosition = new Vector3(xPosition, 0.0f, zPosition);
                // Create a new grid at location
                GameObject tempRoomObj = Instantiate(RandomRoomPrefab(), newPosition, Quaternion.identity) as GameObject;
                // Set its parent
                tempRoomObj.transform.parent = this.transform;
                // Give it a meaningful name
                tempRoomObj.name = "Room_" + column + "," + row;
                // Get the room object
                Room tempRoom = tempRoomObj.GetComponent<Room>();
                // Open the doors
                // If on the bottom row, open north door
                if (row == 0)
                {
                    tempRoom.doorNorth.SetActive(false);
                }
                else if (row == rows - 1)
                {
                    // If on the top row, open the south door
                    tempRoom.doorSouth.SetActive(false);
                }
                else
                {
                    // In middle, so open both doors
                    tempRoom.doorNorth.SetActive(false);
                    tempRoom.doorSouth.SetActive(false);
                }

                // If on the first column, open the east door
                if (column == 0)
                {
                    tempRoom.doorEast.SetActive(false);
                }
                else if (column == columns - 1)
                {
                    // If on the last column row, open the west door
                    tempRoom.doorWest.SetActive(false);
                }
                else
                {
                    // In the middle, so open both doors
                    tempRoom.doorEast.SetActive(false);
                    tempRoom.doorWest.SetActive(false);
                }

                // Save grid array
                grid[column, row] = tempRoom;

                CreatePlayerSpawnPoints();

                SpawnPowerups();
               
                CreateEnemySpawnPoints();

                CreateEnemyWaypoints();

                SpawnPlayerAndEnemies();
            }           
        }  // End of room creation
       
    }
    private void CreatePlayerSpawnPoints()
    {
        /**************** PLAYER SPAWN POINTS SPAWNER ****************/

        // Spawn player spawn points
        Vector3 playerSpawnPosition = new Vector3(xPosition + 10.0f, 1.0f, zPosition + 5.0f);
        GameObject playerSpawner = Instantiate(playerSpawnPoint, playerSpawnPosition, Quaternion.identity) as GameObject;
        //playerSpawner.tag = "Player Spawn Point";
    }
    private void SpawnPowerups()
    {
        /**************** PICKUP SPAWNER ****************/

        // Spawn pickups
        Vector3 pickupSpawnPosition = new Vector3(xPosition + 10.0f, 1.0f, zPosition + 20.0f);
        GameObject pickupSpawner = Instantiate(PowerupSpawnerPrefab, pickupSpawnPosition, Quaternion.identity) as GameObject;
    }
    private void CreateEnemySpawnPoints()
    {
        /**************** ENEMY SPAWN POINTS SPAWNER ****************/
       
        // Spawn enemy spawn points - location in room not random
        int randESPLoc = UnityEngine.Random.Range(-10, 10);
        Vector3 enemySpawnPosition = new Vector3(xPosition + randESPLoc, 10.0f, zPosition + randESPLoc);
        GameObject enemySpawner = Instantiate(enemySpawnPoint, enemySpawnPosition, Quaternion.identity) as GameObject;
    }
    private void CreateEnemyWaypoints()
    {
        /**************** WAYPOINTS SPAWNER ****************/

        // Spawn 2 waypoints in each room
        for (int i = 0; i < 2; i++)
        {
            // Get random num to add to x and 2 positions - this will randomize their location in the room
            // Is counter even or odd ? This forces waypoint 1 (index 0) to use the -15, 0 modifiers, and waypoint 2 (index 1) to use 0, 15
            // This allows for greater separation between the waypoints
            if (espCounter % 2 == 0)
            {
                UnityEngine.Random.InitState(DateToInt(DateTime.UtcNow));
                randWPLoc = UnityEngine.Random.Range(-15, 0);
                randWPLoc2 = UnityEngine.Random.Range(-15, 0);
            }
            else
            {
                UnityEngine.Random.InitState(DateToInt(DateTime.Now));
                randWPLoc = UnityEngine.Random.Range(0, 15);
                randWPLoc2 = UnityEngine.Random.Range(0, 15);
            }

            espCounter++;
            //Debug.Log("Rand 1: " + randWPLoc + " Rand 2: " + randWPLoc2);
            Vector3 waypointSpawnPosition = new Vector3(xPosition + randWPLoc + randWPLoc2, 1.0f, zPosition + randWPLoc + randWPLoc2);
            // Spawns the waypoint but DOES NOT ACCOUNT FOR COLLISION WITH OTHER ROOM OBJECTS - 
            waypointSpawner = Instantiate(waypointSpawnPoint, waypointSpawnPosition, Quaternion.identity) as GameObject;
        }
    }
    private void SpawnPlayerAndEnemies()
    {
        /************************ PLAYER SPAWN IN A RANDOM ROOM ****************************/

        MapGenerator MG = GetComponent<MapGenerator>();
        int mgCols = MG.columns;
        int mgRows = MG.rows;
        int totalRoomsOnMap = mgCols * mgRows;

        // Once all rooms have been generated, spawn the player
        if (playerCounter < 1 && roomGenIterations == totalRoomsOnMap)
        {
            // Get all player spawn points
            GameManager.instance.playerSpawnPoints = GameObject.FindGameObjectsWithTag("Player Spawn Point");
            // Init random seeding
            UnityEngine.Random.InitState(DateToInt(DateTime.Now));
            // Get random int in range between 0 and length of array - this array size is the number of rooms generated
            int randPSLoc = UnityEngine.Random.Range(1, GameManager.instance.playerSpawnPoints.Length);
            // Derive player spawn position from the random spawn location                    
            Vector3 player1SpawnPositiontf = new Vector3(
                GameManager.instance.playerSpawnPoints[randPSLoc].transform.position.x, 
                GameManager.instance.playerSpawnPoints[randPSLoc].transform.position.y, 
                GameManager.instance.playerSpawnPoints[randPSLoc].transform.position.z);
            // Spawn player 1
            player = Instantiate(playerTank, player1SpawnPositiontf, Quaternion.identity) as GameObject;
            player.tag = "Player";
            // Find main camera
            GameManager.instance.p1Camera = GameObject.FindWithTag("MainCamera");
            // Get the follow & rotate component of the MainCamera
            p1camFAR = GameManager.instance.p1Camera.GetComponent<CameraFollowRotate>();
            // Assign it to the player tank (transform)
            p1camFAR.target = player.transform;
            // Increment counter so player doesn't spawn more than once
            playerCounter++;
            // Add to game manager list
            TankData tankData = player.GetComponent<TankData>();
            GameManager.instance.players.Add(tankData);


            /**************** ENEMY TANK SPAWNER ****************/

            // Get all enemy spawn points
            GameManager.instance.enemySpawnPoints = GameObject.FindGameObjectsWithTag("Enemy Spawn Point");

            foreach (var item in enemyTanks)
            {
                // Init random seeding
                //UnityEngine.Random.InitState(DateToInt(DateTime.Now));
                // Get random int in range between 0 and length of array(-1) - this array size is the number of rooms generated
                int randESLoc = UnityEngine.Random.Range(0, GameManager.instance.enemySpawnPoints.Length - 1);
                // Derive enemy spawn position from the random spawn location                    
                Vector3 enemySpawnPositiontf = new Vector3(
                    GameManager.instance.enemySpawnPoints[randESLoc].transform.position.x,
                    GameManager.instance.enemySpawnPoints[randESLoc].transform.position.y,
                    GameManager.instance.enemySpawnPoints[randESLoc].transform.position.z);
                // Spawn enemy
                enemy = Instantiate(enemyTanks[tankCounter], enemySpawnPositiontf, Quaternion.identity) as GameObject;
                enemy.tag = "Enemy";
                // Assign info to the tank so it knows what to target
                // Get controller
                AIController enemyTankAIController = enemy.GetComponent<AIController>();
                TankData eTankData = enemy.GetComponent<TankData>();
                // Get all waypoints
                waypoints = GameObject.FindGameObjectsWithTag("Waypoint");
                
                // Add the just spawned waypoint to the waypoint array that belongs to the enemy tank just spawned
                enemyTankAIController.waypoints[0] = waypoints[randESLoc].transform;
                enemyTankAIController.waypoints[1] = waypoints[randESLoc + 1].transform;
                // Let the enemy know what their target is (player)
                enemyTankAIController.targetTF = player.transform;
                enemyTankAIController.target = player;
                tankCounter++;
                // Add to game manager list
                GameManager.instance.enemies.Add(eTankData);
                //Debug.Log("ENEMY TANK: " + enemy);
            }

            /************************ PLAYER 2 **********************************/
            // Get random int in range between 0 and length of array - this array size is the number of rooms generated
            int randPS2Loc = UnityEngine.Random.Range(1, GameManager.instance.playerSpawnPoints.Length);
            // Derive player spawn position from the random spawn location                    
            Vector3 player2SpawnPositiontf = new Vector3(
                GameManager.instance.playerSpawnPoints[randPSLoc].transform.position.x + 5,
                GameManager.instance.playerSpawnPoints[randPSLoc].transform.position.y,
                GameManager.instance.playerSpawnPoints[randPSLoc].transform.position.z);
            // Spawn player 2
            player = Instantiate(playerTank, player2SpawnPositiontf, Quaternion.identity) as GameObject;
            player.tag = "Player2";
            InputController controller = player.GetComponent<InputController>();
            controller.input = InputController.InputScheme.arrowKeys;
            // Find main camera
            GameManager.instance.p2Camera = GameObject.FindWithTag("P2Camera");
            // Get the follow & rotate component of the MainCamera
            p2camFAR = GameManager.instance.p2Camera.GetComponent<CameraFollowRotate>();
            // Assign it to the player tank (transform)
            p2camFAR.target = player.transform;
            TankData tank2Data = player.GetComponent<TankData>();
            GameManager.instance.players.Add(tank2Data);

        }   // End player and enemy spawn  
    }
    public int DateToInt(DateTime dateToUse)
    {
        // Add up date values and return result
        return dateToUse.Year + dateToUse.Month + dateToUse.Day + dateToUse.Hour + dateToUse.Minute + dateToUse.Second + dateToUse.Millisecond;
    }
    // Returns a random room
    public GameObject RandomRoomPrefab()
    {
        return gridPrefabs[UnityEngine.Random.Range(0, gridPrefabs.Length)];
    }   
}
