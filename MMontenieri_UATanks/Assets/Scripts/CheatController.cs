﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheatController : MonoBehaviour
{
    public PowerupController powCon;
    public PowerUp cheatPowerup;

    // Start is called before the first frame update
    void Start()
    {
        if (powCon == null)
        {
            powCon = gameObject.GetComponent<PowerupController>();
        }
    }

    // Update is called once per frame
    void Update()
    {         
        // HUE keys pressed?
        if (Input.GetKey(KeyCode.H) && Input.GetKey(KeyCode.U) && Input.GetKeyDown(KeyCode.E))
        {
            Debug.Log("HO");
            //Add powers to the tank
            powCon.Add(cheatPowerup);
        }
    }
}
