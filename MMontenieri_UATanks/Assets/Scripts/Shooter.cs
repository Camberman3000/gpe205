﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/************ HANDLES PROJECTILE FIRING ***********/

public class Shooter : MonoBehaviour
{     
    // Tank data
    public TankData tankData;
    // Tank motor
    public TankMotor motor;
    // Character Controller
    private CharacterController characterController;
    // Tank transform 
    public Transform tankTf;
    // Tank muzzle transform
    public Transform tankMuzzle;

    // Projectile prefab     
    public GameObject projectilePF;    

    // Define the projectile
    public GameObject projectile;
    // Sets the life of the projectile
    public float projectileLife = 0.6f;
    // Projectile force
    public float projectileForce;  
    // Can player shoot?
    public bool shootEnabled;
    // Timer counter
    public float fireCounter;
    // Delay between fire time
    [Tooltip("Smaller number = faster firing")]
    public float fireDelay;
    // How much damage the tank does to another object
    public float damagePerHit;

    public AudioClip gunFire;
    private Transform playerTankMuzzle;

    // Start is called before the first frame update
    void Start()
    {
          
    }

    // Update is called once per frame
    void Update()
    {
        // Allow tank to fire if fireCounter >= fireDelay time. 
        fireCounter += Time.deltaTime;
        if (fireCounter >= fireDelay)
        {
            // Reset shoot enabled so player can fire again.
            shootEnabled = true;
            // Reset counter after fireDelay time has elapsed
            fireCounter = 0.0f;
        }
    }

    public void Shoot()
    {
        if (shootEnabled == true)
        {
            GameObject p1 = GameObject.FindWithTag("Player");
            GameObject p2 = GameObject.FindWithTag("Player2");
            
            // Set the muzzle transform so the projectile comes from the correct tank
            if (GameManager.instance.whoShot == "Player")
            {
                playerTankMuzzle = p1.gameObject.transform.Find("Muzzle");
                // Instantiate projectile at the desired position and rotation
                projectile = Instantiate(projectilePF, playerTankMuzzle.position, tankMuzzle.rotation);
            }
            else if (GameManager.instance.whoShot == "Player2")
            {
                playerTankMuzzle = p2.gameObject.transform.Find("Muzzle");
                // Instantiate projectile at the desired position and rotation
                projectile = Instantiate(projectilePF, playerTankMuzzle.position, tankMuzzle.rotation);
            }
            else
            {
                // Instantiate projectile at the desired position and rotation
                projectile = Instantiate(projectilePF, tankMuzzle.position, tankMuzzle.rotation);
            }            
           
            // Move the projectile in the desired direction at the desired force (velocity)
            projectile.GetComponent<Rigidbody>().velocity = tankMuzzle.TransformDirection(Vector3.forward * projectileForce);
            // Destroy projectile after x seconds.
            Destroy(projectile.gameObject, projectileLife);             
            // Play gunshot sound
            GameManager.instance.gameAudio.PlayOneShot(gunFire, GameManager.instance.sfxVolume);
            // Reset the bool for the next shot
            shootEnabled = false;
        }
    }
}
