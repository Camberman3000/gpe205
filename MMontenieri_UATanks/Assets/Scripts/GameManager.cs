﻿using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    // Define GameManager instance as Singleton
    public static GameManager instance;
    public List<InputController> playerControllers;
    public List<TankData> enemies;
    public List<TankData> players;
     
    public GameObject[] playerSpawnPoints;
    public GameObject[] enemySpawnPoints;
    public GameObject start;
    public GameObject options;
    public GameObject game;
    public GameObject gameOver;
    public bool multiplayer = false;

    // Scoring
    public List<ScoreData> scoresList; // Scores list
    public Text p1ScoreText;
    public Text p2ScoreText;
    public Text p1GameOverScoreText;
    public Text p2GameOverScoreText;
    public int player1Score = 0;
    public int player2Score = 0;
    public int enemyScore = 0;
    public Text highScore1Text;
    public Text highScore2Text;
    public Text highScore3Text;
    public int highScore1 = 0;
    public int highScore2 = 0;
    public int highScore3 = 0;
    // Lives remaining
    public Text p1LivesText;
    public Text p2LivesText;   
    public int player1Lives;
    public int player2Lives;
    // Health
    public Text p1HealthText;
    public Text p2HealthText;

    // bool for seeding map of the day    
    public bool mapOfTheDay;
    public Toggle mapOfDayToggle;

    // GAME OPTIONS
    public float sfxVolume;
    public float musicVolume;

    public GameObject p1Camera;
    public GameObject p2Camera;
    private Camera p1CamRef;
    private Camera p2CamRef;
    // The audio source that plays the sound
    public AudioSource gameAudio;
    public AudioClip clickSound;
    private ScoreData m_score;
    public String whoShot;
    //private int counter;
    public int player1LivesRemaining;
    public int player2LivesRemaining;

    // Runs before Start()
    void Awake()
    {
        // Sets this instance of GameManager as the valid (and only) instance - If another instance is created, it will replace the existing one.
        if (instance == null)
        {
            // Set GameManager instance to this instance if none exists
            instance = this;
        }
        else
        {
            // Show error message if another instance of GameManager exists
            Debug.LogError("Error: There can be only one!...GameManager.");
            // Destroy the existing object to make way for the new one
            Destroy(gameObject);
        }
        // Create list of players
        playerControllers = new List<InputController>();
        // Create list of enemies
        enemies = new List<TankData>();
        // Create list of player by tankdata
        players = new List<TankData>();

        m_score = new ScoreData();        
    }

    // Start is called before the first frame update
    void Start()
    {
        // Create scores list
        scoresList = new List<ScoreData>();

        instance.start = GameObject.FindGameObjectWithTag("StartManager");
        instance.options = GameObject.FindGameObjectWithTag("Options");
        instance.game = GameObject.FindGameObjectWithTag("Game");
        instance.gameOver = GameObject.FindGameObjectWithTag("GameOver");
        instance.start.SetActive(true);
        instance.options.SetActive(false);
        instance.game.SetActive(false);
        instance.gameOver.SetActive(false);

        // Set gameAudio
        gameAudio = gameAudio.GetComponent<AudioSource>();
        // Set initial music vol
        gameAudio.volume = musicVolume * 0.1f;
        // Play game music
        gameAudio.Play();
    }

    // Update is called once per frame
    void Update()
    {
        // Proof that player tank data is accessible - Milestone 3
        //Debug.Log("PLayer tank data: " + playerControllers[0].tankData.moveForwardSpeed);
        // Proof that enemy tank data is accessible - Milestone 3
        //Debug.Log("Enemy tank data: " + enemies[0].enemyRef);
        //Debug.Log("Enemy tank data: " + enemies.Count);        

        p1ScoreText.text = player1Score.ToString();
        p2ScoreText.text = player2Score.ToString();

        // Adjust viewable lives remaining. Game ends at "0" lives
        p1LivesText.text = (player1LivesRemaining).ToString();
        p2LivesText.text = (player2LivesRemaining).ToString();       
    }

    public void RespawnPlayer(GameObject player)
    {
        // Seed - This was needed when restarting a game. Forces each player to spawn at a diff location
        if (player.tag == "Player")
        {
            UnityEngine.Random.InitState(DateToInt(DateTime.UtcNow));
            // Get random spawn location from the available points
            int randPSLoc = UnityEngine.Random.Range(1, GameManager.instance.playerSpawnPoints.Length);
            // New player spawn position from the random spawn location                    
            Vector3 playerSpawnPositiontf = new Vector3(
                GameManager.instance.playerSpawnPoints[randPSLoc].transform.position.x,
                GameManager.instance.playerSpawnPoints[randPSLoc].transform.position.y,
                GameManager.instance.playerSpawnPoints[randPSLoc].transform.position.z);
            // Teleport player to the new random spawn point
            player.transform.position = playerSpawnPositiontf;
        }
        else
        {
            UnityEngine.Random.InitState(DateToInt(DateTime.Now));
        }
        if (player.tag == "Enemy")
        {
            UnityEngine.Random.InitState(DateToInt(DateTime.UtcNow));
            // Get random spawn location from the available points
            int randPSLoc = UnityEngine.Random.Range(1, GameManager.instance.enemySpawnPoints.Length);
            // New enemy spawn position from the random spawn location                    
            Vector3 enemySpawnPositiontf = new Vector3(
                GameManager.instance.enemySpawnPoints[randPSLoc].transform.position.x,
                GameManager.instance.enemySpawnPoints[randPSLoc].transform.position.y,
                GameManager.instance.enemySpawnPoints[randPSLoc].transform.position.z);
            // Teleport player to the new random spawn point
            player.transform.position = enemySpawnPositiontf;
        }
       
      
    }

    public int DateToInt(DateTime dateToUse)
    {
        // Add up date values and return result
        return dateToUse.Year + dateToUse.Month + dateToUse.Day + dateToUse.Hour + dateToUse.Minute + dateToUse.Second + dateToUse.Millisecond;
    }

    public void SetPlayerCameras()
    {
        p1Camera = GameObject.FindWithTag("MainCamera");
        p2Camera = GameObject.FindWithTag("P2Camera");

        if (GameManager.instance.multiplayer == false)
        {
            //Debug.Log("SINGLE PLAYER CHOSEN");
            // Find P2 camera and disable it
            if (p2Camera)
            {
                p2Camera.SetActive(false);
                GameObject p2 = GameObject.FindWithTag("Player2");
                //p2.SetActive(false);
            }
        }
        else
        {
            //Debug.Log("MULTI CHOSEN");
            p1CamRef = p1Camera.GetComponent<Camera>();
            p1CamRef.rect = new Rect(0, 0, 1.0f, 0.5f);

            p2CamRef = p2Camera.GetComponent<Camera>();
            p2CamRef.rect = new Rect(0, 0.5f, 1.0f, 0.5f);
        }
    }

    public void ResetGameVars()
    {
        // Reset player health
        foreach (var item in players)
        {
            int index = players.IndexOf(item);
            players[index].currentTankHealth = players[index].maxTankHealth;
        }
        // Reset to inspector set values
        player1LivesRemaining = player1Lives;
        player2LivesRemaining = player2Lives;       
    }

    public void UpdateScores(ScoreData scoreData)
    {
        // Clear the list to start fresh each time
        scoresList.Clear();

        // Build the scores list
        for (int x = 0; x < 10; x++)
        {
            string scoreEntry = "High_Score_" + (x + 1).ToString();
            if (PlayerPrefs.HasKey(scoreEntry))
            {
                ScoreData newScore = new ScoreData();
                newScore.score = PlayerPrefs.GetInt(scoreEntry);
                scoresList.Add(newScore);
            }
        }
        
        // Add the new score to the list that was passed in from the most recent game played
        scoresList.Add(scoreData);

        // Sort the scores, puts lowest at scoresList[0]
        scoresList.Sort();
        // Reverse score order
        scoresList.Reverse();
        // Get 10 scores
        if (scoresList.Count > 10)
        {
            scoresList = scoresList.GetRange(0, 10);
        }

        // Save the scores to the playerprefs
        for (int x = 0; x < scoresList.Count; x++)
        {
            PlayerPrefs.SetInt("High_Score_" + (x + 1).ToString(), scoresList[x].score);
        }
        // Display high scores
        highScore1Text.text = scoresList[0].score.ToString();        
        highScore2Text.text = scoresList[1].score.ToString();         
        highScore3Text.text = scoresList[2].score.ToString();
        // Save playerprefs
        PlayerPrefs.Save();        
    }
}
